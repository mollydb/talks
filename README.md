# talks

Slides, notes, and misc files from presentations.

Unless otherwise noted, all files are licensed Creative Commons Attribution Share Alike 4.0.